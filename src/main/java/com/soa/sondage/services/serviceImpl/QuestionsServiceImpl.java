package com.soa.sondage.services.serviceImpl;


import com.soa.sondage.models.Entity.ChoixEntity;
import com.soa.sondage.models.Entity.QuestionsEntity;
import com.soa.sondage.models.dto.QuestionResponse;
import com.soa.sondage.models.enums.SortCreteria;
import com.soa.sondage.repository.QuestionsRepository;
import com.soa.sondage.services.service.ChoixService;
import com.soa.sondage.services.service.QuestionsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Primary
@Service
@Slf4j
public class QuestionsServiceImpl implements QuestionsService {

    private final QuestionsRepository questionsRepository;
    private final ChoixService choixService;

    /**
     * @param questionsRepository
     * @param choixService choix service to do some operation on choix
     */
    @Autowired
    public QuestionsServiceImpl(QuestionsRepository questionsRepository, ChoixService choixService) {
        this.questionsRepository = questionsRepository;
        this.choixService = choixService;
    }

    /**
     * @param question
     * @return created question with all associated choix
     */
    public QuestionsEntity createQuestion(QuestionsEntity question) {
        question.getChoix().replaceAll(this.choixService::createChoix);

        return this.saveQuestion(question);
    }

    /**
     * @param id
     * @param choix
     * @return List of all added choix (not all choix of a question)
     */
    @Override
    public List<ChoixEntity> addChoix(int id, List<ChoixEntity> choix) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);
        if(questionsEntity == null) {
            return null;
        }

        choix.replaceAll(this.choixService::createChoix);
        questionsEntity.getChoix().addAll(choix);
        this.saveQuestion(questionsEntity);

        return choix;
    }

    /**
     * @return all questions (id, libelle and datePub, nombreDeChoix), we don't return the list of choix
     */
    @Override
    public List<QuestionResponse> getAllQuestions() {
        List<QuestionsEntity> questions = (List<QuestionsEntity>) this.questionsRepository.findAll();

        return questions.stream().map(this::mappQuestionToQuestionResponse).collect(Collectors.toList());
    }

    @Override
    public QuestionsEntity getQuestion(int id) {
        return this.getQuestionById(id);
    }

    @Override
    public List<ChoixEntity> getAllChoix(int id) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);
        if(questionsEntity == null) {
            return null;
        }

        return questionsEntity.getChoix();
    }

    @Override
    public ChoixEntity getChoix(int id, int idChoix) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);
        ChoixEntity choixEntity = this.choixService.getChoixById(idChoix);
        assert questionsEntity != null;
        if(questionsEntity.getChoix().contains(choixEntity)){
            return choixEntity;
        }

        return null;
    }

    @Override
    public QuestionResponse updateQuestion(int id, QuestionsEntity question) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);
        if(questionsEntity == null) {
            return null;
        }

        questionsEntity.setLibelle(question.getLibelle());
        questionsEntity.setDatePub(question.getDatePub());
        this.questionsRepository.save(questionsEntity);

        return this.mappQuestionToQuestionResponse(questionsEntity);
    }

    @Override
    public ChoixEntity updateChoix(int id, int idChoix, ChoixEntity choix) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);
        if (questionsEntity == null) {
            return null;
        }

        ChoixEntity choixEntity = this.choixService.getChoixById(id);
        if(choixEntity == null) {
            return null;
        }

        if(!questionsEntity.getChoix().contains(choixEntity)) {
            return null;
        }

        return this.choixService.updateChoix(choixEntity, choix);
    }

    @Override
    public void deleteQuestion(int id) {
        this.questionsRepository.deleteById(id);
    }

    @Override
    public void deleteChoix(int id, int idChoix) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);

        if (questionsEntity != null) {
            ChoixEntity choixEntity = this.choixService.getChoixById(idChoix);
            if (questionsEntity.getChoix().contains(choixEntity)) {
                questionsEntity.getChoix().remove(choixEntity);
                this.questionsRepository.save(questionsEntity);
                this.choixService.deleteById(idChoix);
            }
        }
    }

    @Override
    public List<QuestionResponse> getSortedQuestion(String sort) {
        List<QuestionsEntity> questions = (List<QuestionsEntity>) this.questionsRepository.findAll();
        if(sort.equals(SortCreteria.ASC.toString())) {
            Collections.sort(questions, Comparator.comparing(QuestionsEntity::getDatePub));
        } else if(sort.equals(SortCreteria.DESC.toString())) {
            Collections.sort(questions, Comparator.comparing(QuestionsEntity::getDatePub).reversed());
        } else {
            return null;
        }

        return questions.stream().map(this::mappQuestionToQuestionResponse).collect(Collectors.toList());
    }

    @Override
    public List<QuestionsEntity> getAllSortedQuestionsWithChoix(String sort) {
        List<QuestionsEntity> questions = (List<QuestionsEntity>) this.questionsRepository.findAll();
        if(sort.equals(SortCreteria.ASC.toString())) {
            Collections.sort(questions, Comparator.comparing(QuestionsEntity::getDatePub));
        } else if(sort.equals(SortCreteria.DESC.toString())) {
            Collections.sort(questions, Comparator.comparing(QuestionsEntity::getDatePub).reversed());
        } else {
            return null;
        }

        return questions;
    }

    @Override
    public ChoixEntity vote(int id, int idChoix) {
        QuestionsEntity questionsEntity = this.getQuestionById(id);

        if (questionsEntity != null) {
            ChoixEntity choixEntity = this.choixService.getChoixById(idChoix);
            if (questionsEntity.getChoix().contains(choixEntity)) {
                this.choixService.addVote(choixEntity);
            }

            return this.choixService.getChoixById(idChoix);
        }

        return null;
    }

    private QuestionsEntity saveQuestion(QuestionsEntity questionsEntity) {
        return this.questionsRepository.save(questionsEntity);
    }

    private QuestionsEntity getQuestionById(int id) {
        Optional<QuestionsEntity> questionSearched = this.questionsRepository.findById(id);
        if(questionSearched.isEmpty()) {
            return null;
        }

        return questionSearched.get();
    }

    private QuestionResponse mappQuestionToQuestionResponse(QuestionsEntity questionsEntity) {
        QuestionResponse questionResponse = new QuestionResponse();
        questionResponse.setId(questionsEntity.getId());
        questionResponse.setLibelle(questionsEntity.getLibelle());
        questionResponse.setDatePub(questionsEntity.getDatePub());
        questionResponse.setNombreDeChoix(questionsEntity.getChoix().stream().count());

        return questionResponse;
    }
}
