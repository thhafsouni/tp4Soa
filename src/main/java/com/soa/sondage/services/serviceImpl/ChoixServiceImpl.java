package com.soa.sondage.services.serviceImpl;

import com.soa.sondage.models.Entity.ChoixEntity;
import com.soa.sondage.repository.ChoixRepository;
import com.soa.sondage.services.service.ChoixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class ChoixServiceImpl implements ChoixService {

    private final ChoixRepository choixRepository;

    @Autowired
    public ChoixServiceImpl(ChoixRepository choixRepository) {
        this.choixRepository = choixRepository;
    }

    @Override
    public ChoixEntity createChoix(ChoixEntity choixEntity) {
        return this.choixRepository.save(choixEntity);
    }

    @Override
    public ChoixEntity getChoixById(int idChoix) {
        return this.choixRepository.findById(idChoix).get();
    }

    @Override
    public ChoixEntity updateChoix(ChoixEntity choixEntity, ChoixEntity choix) {
        return this.choixRepository.save(this.mapping(choixEntity, choix));
    }

    @Override
    public ChoixEntity addVote(ChoixEntity choix) {
        choix.setVote(choix.getVote() + 1);
        return this.choixRepository.save(choix);
    }

    @Override
    public void deleteById(int idChoix) {
        this.choixRepository.deleteById(idChoix);
    }

    private ChoixEntity mapping(ChoixEntity choixEntity, ChoixEntity choix) {
        choixEntity.setLibelle(choix.getLibelle());
        choixEntity.setVote(choix.getVote());

        return choixEntity;
    }
}
