package com.soa.sondage.services.service;

import com.soa.sondage.models.Entity.ChoixEntity;
import com.soa.sondage.models.Entity.QuestionsEntity;
import com.soa.sondage.models.dto.QuestionResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface QuestionsService {
    public QuestionsEntity createQuestion(QuestionsEntity question);

    public List<ChoixEntity> addChoix(int id, List<ChoixEntity> choix);

    public List<QuestionResponse> getAllQuestions();

    public QuestionsEntity getQuestion(int id);

    public List<ChoixEntity> getAllChoix(int id);

    public ChoixEntity getChoix(int id, int idChoix);

    public QuestionResponse updateQuestion(int id, QuestionsEntity question);

    public ChoixEntity updateChoix(int id, int idChoix, ChoixEntity choix);

    public void deleteQuestion(int id);

    public void deleteChoix(int id, int idChoix);

    public List<QuestionResponse> getSortedQuestion(String sort);

    public List<QuestionsEntity> getAllSortedQuestionsWithChoix(String sort);

    public ChoixEntity vote(int id, int idChoix);
}
