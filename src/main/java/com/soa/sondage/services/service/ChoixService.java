package com.soa.sondage.services.service;

import com.soa.sondage.models.Entity.ChoixEntity;
import org.springframework.stereotype.Service;

@Service
public interface ChoixService {


    public ChoixEntity createChoix(ChoixEntity choixEntity);

    public ChoixEntity getChoixById(int idChoix);

    public ChoixEntity updateChoix(ChoixEntity choixEntity, ChoixEntity choix);

    public ChoixEntity addVote(ChoixEntity choix);

    public void deleteById(int idChoix);
}
