package com.soa.sondage.models.enums;

import lombok.Data;

public enum SortCreteria {

    ASC("asc"),
    DESC("desc");

    private final String creteria;

    private SortCreteria(String creteria) {
        this.creteria = creteria;
    }

    @Override
    public String toString() {
        return this.creteria;
    }
}
