package com.soa.sondage.models.Entity;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Data
public class QuestionsEntity implements Serializable {

    private  static  final  long serialVersionUID = -7806505029375454508L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String libelle;

    private LocalDateTime datePub;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id")
    private List<ChoixEntity> choix;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionsEntity that = (QuestionsEntity) o;
        return id == that.id && Objects.equals(libelle, that.libelle) && Objects.equals(datePub, that.datePub) && Objects.equals(choix, that.choix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, datePub, choix);
    }
}
