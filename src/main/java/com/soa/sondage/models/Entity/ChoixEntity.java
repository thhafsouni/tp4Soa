package com.soa.sondage.models.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Data
public class ChoixEntity implements Serializable {

    private  static  final  long serialVersionUID = 5325916464042603949L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String libelle;

    private int vote;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChoixEntity that = (ChoixEntity) o;
        return id == that.id && vote == that.vote && Objects.equals(libelle, that.libelle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, vote);
    }
}
