package com.soa.sondage.models.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QuestionResponse implements Serializable {

    private  static  final  long serialVersionUID = -5894610536162458194L;

    private int id;

    private String libelle;

    private LocalDateTime datePub;

    private Long nombreDeChoix;
}
