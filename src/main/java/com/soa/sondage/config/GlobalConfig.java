package com.soa.sondage.config;

import com.soa.sondage.repository.ChoixRepository;
import com.soa.sondage.repository.QuestionsRepository;
import com.soa.sondage.services.service.ChoixService;
import com.soa.sondage.services.service.QuestionsService;
import com.soa.sondage.services.serviceImpl.ChoixServiceImpl;
import com.soa.sondage.services.serviceImpl.QuestionsServiceImpl;
import jdk.jfr.Category;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConfig {

    @Bean
    public QuestionsService questionsService(QuestionsRepository questionsRepository, ChoixService choixService) {
        return new QuestionsServiceImpl(questionsRepository, choixService);
    }

    @Bean
    public ChoixService choixService(ChoixRepository choixRepository) {
        return new ChoixServiceImpl(choixRepository);
    }

}
