package com.soa.sondage.repository;

import com.soa.sondage.models.Entity.ChoixEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ChoixRepository extends CrudRepository<ChoixEntity, Integer> {

}
