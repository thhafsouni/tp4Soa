package com.soa.sondage.repository;

import com.soa.sondage.models.Entity.QuestionsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionsRepository extends CrudRepository<QuestionsEntity, Integer> {
}
