package com.soa.sondage.api;

import com.soa.sondage.models.Entity.ChoixEntity;
import com.soa.sondage.models.Entity.QuestionsEntity;
import com.soa.sondage.models.dto.QuestionResponse;
import jdk.jfr.Unsigned;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "/sondage")
public interface SondageApi {

    @PostMapping("/questions")
    public ResponseEntity<QuestionsEntity> createQuestion(
            @RequestBody QuestionsEntity question);

    @PostMapping("/questions/{id}/choix")
    public ResponseEntity<List<ChoixEntity>> addChoix(@PathVariable int id,
                                                      @RequestBody List<ChoixEntity> choix);

    @GetMapping("/questions")
    public ResponseEntity<List<QuestionResponse>> getAllQuestions();

    @GetMapping("/questions/{id}")
    public ResponseEntity<QuestionsEntity> getQuestion(@PathVariable int id);

    @GetMapping("/questions/{id}/choix")
    public ResponseEntity<List<ChoixEntity>> getAllChoix(@PathVariable int id);

    @GetMapping("/questions/{id}/choix/{idChoix}")
    public ResponseEntity<ChoixEntity> getChoix(@PathVariable int id,
                                                @PathVariable int idChoix);

    @PutMapping("/questions/{id}")
    public ResponseEntity<QuestionResponse> updateQuestion(@PathVariable int id,
                                                           @RequestBody QuestionsEntity question);

    @PutMapping("/questions/{id}/choix/{idChoix}")
    public ResponseEntity<ChoixEntity> updateChoix(@PathVariable int id,
                                                   @PathVariable int idChoix,
                                                   @RequestBody ChoixEntity choix);

    @DeleteMapping("/questions/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable int id);

    @DeleteMapping("/questions/{id}/choix/{idChoix}")
    public ResponseEntity<Void> deleteChoix(@PathVariable int id,
                                                   @PathVariable int idChoix);

    @GetMapping("/questions/sort")
    public ResponseEntity<List<QuestionResponse>> getSortedQuesttions(@RequestParam String sort);

    @PutMapping("/questions/{id}/choix/{idChoix}/vote")
    public ResponseEntity<ChoixEntity> vote(@PathVariable int id,
                                            @PathVariable int idChoix);

    @GetMapping("/questions/results/sort")
    public ResponseEntity<List<QuestionsEntity>> getSortedQuestion(@RequestParam String sort);
}
