package com.soa.sondage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;

@SpringBootApplication
public class SondageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SondageApplication.class, args);
    }

}
