package com.soa.sondage.controllers;

import com.soa.sondage.api.SondageApi;
import com.soa.sondage.models.Entity.ChoixEntity;
import com.soa.sondage.models.Entity.QuestionsEntity;
import com.soa.sondage.models.dto.QuestionResponse;
import com.soa.sondage.services.service.QuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SondageController implements SondageApi {

    @Autowired
    private QuestionsService questionsService;

    @Override
    public ResponseEntity<QuestionsEntity> createQuestion(QuestionsEntity question) {
        QuestionsEntity questionsEntity = this.questionsService.createQuestion(question);

        return new ResponseEntity<>(
                questionsEntity, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ChoixEntity>> addChoix(int id, List<ChoixEntity> choix) {
        List<ChoixEntity> choixList = this.questionsService.addChoix(id, choix);
        return new ResponseEntity<>(choixList,
                HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<QuestionResponse>> getAllQuestions() {
        List<QuestionResponse> responses = this.questionsService.getAllQuestions();
        if(responses == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<QuestionsEntity> getQuestion(int id) {
        QuestionsEntity response = this.questionsService.getQuestion(id);
        if(response == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ChoixEntity>> getAllChoix(int id) {
        List<ChoixEntity> responses = this.questionsService.getAllChoix(id);
        if(responses == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChoixEntity> getChoix(int id, int idChoix) {
        ChoixEntity choixEntity = this.questionsService.getChoix(id, idChoix);
        if(choixEntity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(choixEntity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<QuestionResponse> updateQuestion(int id, QuestionsEntity question) {
        QuestionResponse questionResponse = this.questionsService.updateQuestion(id, question);
        if(questionResponse == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(questionResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChoixEntity> updateChoix(int id, int idChoix, ChoixEntity choix) {
        ChoixEntity choixEntity = this.questionsService.updateChoix(id, idChoix, choix);
        if(choixEntity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(choixEntity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteQuestion(int id) {
        this.questionsService.deleteQuestion(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteChoix(int id, int idChoix) {
        this.questionsService.deleteChoix(id, idChoix);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<QuestionResponse>> getSortedQuesttions(String sort) {
        List<QuestionResponse> responses = this.questionsService.getSortedQuestion(sort);
        if(responses == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChoixEntity> vote(int id, int idChoix) {
        ChoixEntity choixEntity = this.questionsService.vote(id, idChoix);
        if(choixEntity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(choixEntity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<QuestionsEntity>> getSortedQuestion(String sort) {
        List<QuestionsEntity> responses = this.questionsService.getAllSortedQuestionsWithChoix(sort);
        if(responses == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

}
